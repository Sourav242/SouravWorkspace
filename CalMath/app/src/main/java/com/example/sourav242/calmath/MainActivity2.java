package com.example.sourav242.calmath;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity2 extends GameMenu {

    String packageName = "com.example.sourav242.calmath";
    int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("close", false)) {
            kill();
        }
        else
            setContentView(R.layout.activity_main2);
    }

    public void kill(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    public void clk1p(View v){
        Button btn = (Button)v;

        SessionClass Session = (SessionClass)getApplicationContext();

        if(btn.getText().toString().contains("1")) {

            /*String className = "com.example.sourav242.getthat_20.Toss";
            String packageName = "com.example.sourav242.getthat_20";

            Intent intent = new Intent();
            intent.setClassName(packageName, className);

            startActivity(intent);*/

            Session.setPlayer("1P");
            Intent i = new Intent(v.getContext(), Toss.class);
            startActivity(i);
            //finish();
        }
        if(btn.getText().toString().contains("2")) {
            Session.setPlayer("2P");
            Intent i = new Intent(v.getContext(), Game1_1.class);
            startActivity(i);
            //finish();
        }
    }

    public void alertShow(String s){
        //Game Ended...!!!
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);

        // Setting Dialog Title
        if(s.equals("You") || (s.contains("P")))
            alertDialog.setTitle("Congratulations...");
        else
            alertDialog.setTitle("Game's Up...");

        // Setting Dialog Message
        if(s.equals("You") || s.contains("P"))
            alertDialog.setMessage(s+" Win The Game...");
        else
            alertDialog.setMessage("You Lose... I Win...");

        final ImageView img = new ImageView(this);
        if(s.equals("You") || (s.contains("P")))
            img.setImageResource(R.drawable.firecrackers);
        else
            img.setImageResource(R.drawable.mmm);

        alertDialog.setView(img);

        // Setting Dialog Cancellation
        alertDialog.setCancelable(false);

        // Setting Positive "Yes" Button

        SessionClass sessionClass = (SessionClass)getApplicationContext();
        final String str = sessionClass.getPlayer();
        alertDialog.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent i;
                if(null != str && str.contains("2")){
                    i = new Intent(MainActivity2.this, Game1_1.class);
                    startActivity(i);
                }
                finish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MainActivity2.this, MainActivity2.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |  Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void exitAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Exit");

        // Setting Dialog Message
        if(this.getClass().getSimpleName().equals("Game1") || this.getClass().getSimpleName().equals("Game1_1")) {
            alertDialog.setMessage("Are you sure to stop playing and quit the Game?");
        }
        else {
            alertDialog.setMessage("Do you want to quit the Game?");
        }

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(!this.getClass().getSimpleName().equals("MainActivity2")) {
                    Intent intent = new Intent(MainActivity2.this, GameMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.putExtra("close", true);
                    startActivity(intent);
                    finish();
                }
                kill();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy The Game", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void alertBack(String x){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Wait!!!");
        alertDialog.setMessage("Are you sure to "+x+" the game?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy the Game", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
