package com.example.sourav242.calmath;

import android.provider.BaseColumns;

/**
 * Created by Sourav242 on 21-02-2016.
 */
public class TableData {

    public TableData() {}

    public static abstract class TableInfo implements BaseColumns {

        public static final String GAME_NAME = "game_name";
        public static final String HI_SCORE = "hi_score";
        public static final String DATABASE_NAME = "score_info";
        public static final String TABLE_NAME = "hi_score";
    }
}
