package com.example.sourav242.calmath;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

public class GetThat20 extends GameMenu {

    Handler h = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            final Intent mainIntent = new Intent(GetThat20.this, MainActivity2.class);
            startActivity(mainIntent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_that_20);
        h.postDelayed(r, 3000);
    }

    public void onSplash2(View view) {
        h.removeCallbacks(r);
        Intent mainIntent = new Intent(this, MainActivity2.class);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        h.removeCallbacks(r);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        h.removeCallbacks(r);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        h.removeCallbacks(r);
        h.postDelayed(r,500);
    }
}
