package com.example.sourav242.calmath;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Game1 extends MainActivity2 {

    String s;
    String s1;
    int result;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game1);
        SessionClass Session = (SessionClass)getApplicationContext();
        s = "";

        TextView t = (TextView)findViewById(R.id.textView);
        t.setText(Session.getMode()+"\n\nYou : ");
        s = t.getText().toString();
        if(Session.getMode().contains("Let")) {
            Random rn = new Random();
            int n = rn.nextInt(3)+1;
            int i;
            t.setText("Reach 20\n\nSmarty : ");
            s = t.getText().toString();
            for(i=1; i<=n; i++)
            {
                t.setText(s+i+" ");
                s = t.getText().toString();
            }
            s = s+"\nYou : ";
            t.setText(s);
            int j = i;
            TextView text1 = (TextView) findViewById(R.id.num1);
            TextView text2 = (TextView) findViewById(R.id.num2);
            TextView text3 = (TextView) findViewById(R.id.num3);
            text1.setText(Integer.toString(j));
            text2.setText(Integer.toString(++j));
            text3.setText(Integer.toString(++j));
        }
    }

    public void play(View v) {
        TextView clk = (TextView) v;

        if(clk.getText().toString().equals("Start New")){
            alertBack("restart");
        }
        else {
            TextView setText = (TextView) findViewById(R.id.textView);
            TextView text1 = (TextView) findViewById(R.id.num1);
            TextView text2 = (TextView) findViewById(R.id.num2);
            TextView text3 = (TextView) findViewById(R.id.num3);
            switch (v.getId()) {
                case R.id.num1:
                    setText.setText(s + text1.getText().toString());
                    break;
                case R.id.num2:
                    setText.setText(s + text1.getText().toString() + " " + text2.getText().toString());
                    break;
                case R.id.num3:
                    setText.setText(s + text1.getText().toString() + " " + text2.getText().toString() + " " + text3.getText().toString());
                    break;
            }
        }
    }

    public void proceed(View v){
        TextView t = (TextView)findViewById(R.id.textView);
        if(s.trim().equals(t.getText().toString().trim()) || (s+"You : ".trim()).equals(t.getText().toString().trim()) || t.getText().toString().trim().equals("Reach 20\n\nYou :")){
            Toast.makeText(getBaseContext(), "Please Enter Something", Toast.LENGTH_SHORT).show();
        }
        else {
            t.setMovementMethod(new ScrollingMovementMethod());
            s = t.getText().toString().trim();
            int j = s.lastIndexOf(":");
            j += 2;
            s1 = s.substring(j);
            result = s1.lastIndexOf(" ");
            s1 = s1.substring(result + 1);
            result = Integer.parseInt(s1);       //Last value of the gamer
            s += "\nSmarty : ";
            t.setText(s);
            if (result % 4 == 0) {
                if (result == 20) {
                    result = 0;
                    flag = 1;
                    alertShow("You");
                }
                else {
                    Random rn = new Random();
                    int x = rn.nextInt(3) + 1;
                    for (j = 1; j <= x; j++) {
                        ++result;
                        s += result + " ";
                        t.setText(s);
                    }
                    t.setText(s);
                }
            }
            else {
                if(result<=4){
                    Random rn = new Random();
                    int x = rn.nextInt(3) + 1;
                    for (j = 1; j <= x; j++) {
                        ++result;
                        s += result + " ";
                        t.setText(s);
                    }
                }
                else {
                    do {
                        result++;
                        s += result + " ";
                        t.setText(s);
                    }
                    while (result % 4 != 0);
                }
            }
            if (result == 20) {
                result = 0;
                flag = 2;
                alertShow("l");
            }
            if (result != 0) {
                s += "\nYou : ";
                t.setText(s);
                j = result;
                TextView text1 = (TextView) findViewById(R.id.num1);
                TextView text2 = (TextView) findViewById(R.id.num2);
                TextView text3 = (TextView) findViewById(R.id.num3);
                RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.relativelayout);
                j++;
                if (j <= 20)
                    text1.setText(Integer.toString(j));
                else {
                    text1.setText("");
                    text1.setBackgroundColor(relativeLayout.getDrawingCacheBackgroundColor());
                }
                j++;
                if (j <= 20)
                    text2.setText(Integer.toString(j));
                else {
                    text2.setText("");
                    text2.setBackgroundColor(relativeLayout.getDrawingCacheBackgroundColor());
                }
                j++;
                if (j <= 20)
                    text3.setText(Integer.toString(j));
                else {
                    text3.setText("");
                    text3.setBackgroundColor(relativeLayout.getDrawingCacheBackgroundColor());
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView t = (TextView)findViewById(R.id.textView);
        TextView text1 = (TextView) findViewById(R.id.num1);
        TextView text2 = (TextView) findViewById(R.id.num2);
        TextView text3 = (TextView) findViewById(R.id.num3);
        outState.putString("text",t.getText().toString());
        outState.putString("num1",text1.getText().toString());
        outState.putString("num2",text2.getText().toString());
        outState.putString("num3",text3.getText().toString());
        outState.putInt("flag", flag);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView t = (TextView)findViewById(R.id.textView);
        TextView text1 = (TextView) findViewById(R.id.num1);
        TextView text2 = (TextView) findViewById(R.id.num2);
        TextView text3 = (TextView) findViewById(R.id.num3);
        text1.setText(savedInstanceState.getString("num1"));
        text2.setText(savedInstanceState.getString("num2"));
        text3.setText(savedInstanceState.getString("num3"));
        String x = savedInstanceState.getString("text");
        int p = x.lastIndexOf(":");
        x = x.substring(0, p+1);
        t.setText(x+" ");
        s = x+" ";
        flag = savedInstanceState.getInt("flag");
        if(flag == 1)
            alertShow("You");
        else if(flag == 2)
            alertShow("1");
        t.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onBackPressed() {
        alertBack("exit");
    }
}
