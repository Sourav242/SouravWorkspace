package com.example.sourav242.calmath;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Sourav242 on 21-02-2016.
 */
public class DatabaseOperations extends SQLiteOpenHelper {

    public static final int database_version = 1;
    public String CREATE_QUERY = "CREATE TABLE "+ TableData.TableInfo.TABLE_NAME+" ("+ TableData.TableInfo.GAME_NAME+" TEXT, "+ TableData.TableInfo.HI_SCORE+" TEXT)";

    public DatabaseOperations(Context context) {
        super(context, TableData.TableInfo.DATABASE_NAME, null, database_version);
        Log.d("Database Operations", "Database Successfully Created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
        Log.d("Database Operations", "Table Successfully Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getCount() {
        String countQuery = "SELECT  * FROM " + TableData.TableInfo.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public void putInformation(DatabaseOperations dop, String name, String pass) {
        SQLiteDatabase SQ = dop.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableData.TableInfo.GAME_NAME, name);
        cv.put(TableData.TableInfo.HI_SCORE, pass);
        long k = SQ.insert(TableData.TableInfo.TABLE_NAME, null, cv);
        Log.d("Database Operations", "One Row Inserted Successfully");
    }

    public Cursor getInformation(DatabaseOperations dob) {
        SQLiteDatabase SQ = dob.getReadableDatabase();
        String[] columns = {TableData.TableInfo.GAME_NAME, TableData.TableInfo.HI_SCORE};
        return SQ.query(TableData.TableInfo.TABLE_NAME, columns, null, null, null, null, null);
    }

    public void updateUserInfo(DatabaseOperations DOB, String oun, String oup, String nun, String nup) {
        SQLiteDatabase SQ = DOB.getWritableDatabase();
        String query = TableData.TableInfo.GAME_NAME+" LIKE ? AND "+ TableData.TableInfo.HI_SCORE+" LIKE ?";
        String args[] = {oun,oup};
        ContentValues values = new ContentValues();
        values.put(TableData.TableInfo.GAME_NAME,nun);
        values.put(TableData.TableInfo.HI_SCORE,nup);
        SQ.update(TableData.TableInfo.TABLE_NAME,values,query,args);
        Log.d("Database Operations", "Updation Successfully");
    }
}
