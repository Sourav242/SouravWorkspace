package com.example.sourav242.calmath;

import android.app.Application;

public class SessionClass extends Application {
    public String Player;
    public String Mode;

    public void setPlayer(String player){
        Player = player;
    }
    public void setMode(String mode){
        Mode = mode;
    }
    public String getPlayer(){
        return Player;
    }
    public String getMode(){
        return Mode;
    }
}
