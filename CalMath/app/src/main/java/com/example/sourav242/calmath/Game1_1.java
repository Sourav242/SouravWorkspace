package com.example.sourav242.calmath;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Game1_1 extends MainActivity2 {

    String s,s1;
    int k;
    int result;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game1_1);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout);
        relativeLayout.setBackgroundColor(Color.parseColor("#99cc22"));
        s = "";
        TextView t = (TextView)findViewById(R.id.textView);
        t.setText("Reach 20\n\nPlayer1 :");
        s = "Reach 20\n\n";
    }

    public void play2(View v) {
        TextView clk = (TextView) v;
        s1 = s;

        if(clk.getText().toString().equals("Start New")){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Wait!!!");
            alertDialog.setMessage("Are you sure to restart the game?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(Game1_1.this, Game1_1.class);
                    startActivity(i);
                    finish();
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getBaseContext(), "Enjoy the Game", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
        else {
            TextView setText = (TextView) findViewById(R.id.textView);
            TextView text1 = (TextView) findViewById(R.id.num1);
            TextView text2 = (TextView) findViewById(R.id.num2);
            TextView text3 = (TextView) findViewById(R.id.num3);
            if(k%2==0) {
                switch (v.getId()) {
                    case R.id.num1:
                        setText.setText(s + "Player1 : " + text1.getText().toString());
                        break;
                    case R.id.num2:
                        setText.setText(s + "Player1 : " + text1.getText().toString() + " " + text2.getText().toString());
                        break;
                    case R.id.num3:
                        setText.setText(s + "Player1 : " + text1.getText().toString() + " " + text2.getText().toString() + " " + text3.getText().toString());
                        break;
                }
            }
            else {
                switch (v.getId()) {
                    case R.id.num1:
                        setText.setText(s + "Player2 : " + text1.getText().toString());
                        break;
                    case R.id.num2:
                        setText.setText(s + "Player2 : " + text1.getText().toString() + " " + text2.getText().toString());
                        break;
                    case R.id.num3:
                        setText.setText(s + "Player2 : " + text1.getText().toString() + " " + text2.getText().toString() + " " + text3.getText().toString());
                        break;
                }
            }
        }
    }

    public void proceed2 (View v){
        TextView t = (TextView)findViewById(R.id.textView);
        if(t.getText().toString().trim().substring(t.getText().toString().trim().length()-1).equals(":") || t.getText().toString().trim().equals("Reach 20\n\nYou :")){
            Toast.makeText(getBaseContext(), "Please Enter Something", Toast.LENGTH_SHORT).show();
        }
        else {
            k++;
            t.setMovementMethod(new ScrollingMovementMethod());
            s = t.getText().toString().trim();
            int j = s.lastIndexOf(":");
            j += 2;
            s1 = s.substring(j);
            result = s1.lastIndexOf(" ");
            s1 = s1.substring(result + 1);
            result = Integer.parseInt(s1);
            if (result == 20) {
                if (k % 2 == 0) {
                    String sss = "Player2";
                    flag = 2;
                    alertShow(sss);
                }
                else {
                    String sss = "Player1";
                    flag = 1;
                    alertShow(sss);
                }
            }
            s += "\n";
            j = result;
            TextView text1 = (TextView) findViewById(R.id.num1);
            TextView text2 = (TextView) findViewById(R.id.num2);
            TextView text3 = (TextView) findViewById(R.id.num3);
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout);
            j++;
            if (j <= 20)
                text1.setText(Integer.toString(j));
            else {
                text1.setText("");
                text1.setBackgroundColor(relativeLayout.getDrawingCacheBackgroundColor());
            }
            j++;
            if (j <= 20)
                text2.setText(Integer.toString(j));
            else {
                text2.setText("");
                text2.setBackgroundColor(relativeLayout.getDrawingCacheBackgroundColor());
            }
            j++;
            if (j <= 20)
                text3.setText(Integer.toString(j));
            else {
                text3.setText("");
                text3.setBackgroundColor(relativeLayout.getDrawingCacheBackgroundColor());
            }
            if (k % 2 != 0) {
                t.setText(s + "Player2 : ");
                if(result<20) {
                    relativeLayout.setBackgroundColor(Color.parseColor("#6495ed"));
                }
            }
            else {
                t.setText(s + "Player1 : ");
                if(result<20) {
                    relativeLayout.setBackgroundColor(Color.parseColor("#99cc22"));
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView t = (TextView)findViewById(R.id.textView);
        TextView text1 = (TextView) findViewById(R.id.num1);
        TextView text2 = (TextView) findViewById(R.id.num2);
        TextView text3 = (TextView) findViewById(R.id.num3);
        outState.putString("text",t.getText().toString());
        outState.putString("num1",text1.getText().toString());
        outState.putString("num2",text2.getText().toString());
        outState.putString("num3",text3.getText().toString());
        outState.putInt("k",k);
        outState.putInt("flag",flag);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView t = (TextView)findViewById(R.id.textView);
        TextView text1 = (TextView) findViewById(R.id.num1);
        TextView text2 = (TextView) findViewById(R.id.num2);
        TextView text3 = (TextView) findViewById(R.id.num3);
        k = savedInstanceState.getInt("k");
        text1.setText(savedInstanceState.getString("num1"));
        text2.setText(savedInstanceState.getString("num2"));
        text3.setText(savedInstanceState.getString("num3"));
        String x = savedInstanceState.getString("text");
        int p = x.lastIndexOf(":");
        t.setText(x.substring(0, p)+": ");
        x = x.substring(0, p-8);
        s = x;
        flag = savedInstanceState.getInt("flag");
        if(flag == 1)
            alertShow("Player1");
        else if(flag == 2)
            alertShow("Player2");
        t.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onBackPressed() {
        alertBack("exit");
    }
}
