package com.example.sourav242.calmath;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@TargetApi(Build.VERSION_CODES.FROYO)
@SuppressLint("NewApi")

public class Game3 extends GameMenu {

    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> a1 = new ArrayList<>();
    ArrayList<String> a2 = new ArrayList<>();
    ArrayList<String> a3 = new ArrayList<>();
    ArrayList<String> a4 = new ArrayList<>();
    double ans;
    int n;
    CounterClass timer;
    MultiTouchListener touchListener;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game3);
        counter = 1;
        RelativeLayout r = (RelativeLayout)findViewById(R.id.background);
        r.setBackgroundColor(Color.parseColor("#ffffff"));

        DatabaseOperations DB = new DatabaseOperations(ctx);
        int c = DB.getCount();
        if(c>0) {
            Cursor cr = DB.getInformation(DB);
            cr.moveToFirst();
            if(c==1 && "game3".equals(cr.getString(0))) {
                String hs = "";
                do {
                    if ("game3".equals(cr.getString(0))) {
                        hs = cr.getString(1);
                    }
                } while (cr.moveToNext());
                TextView hi = (TextView) findViewById(R.id.textView4);
                hi.setText("Hi Score : " + hs);
            }
            if(c==2){
                String hs = "";
                do {
                    if ("game3".equals(cr.getString(0))) {
                        hs = cr.getString(1);
                    }
                } while (cr.moveToNext());
                TextView hi = (TextView) findViewById(R.id.textView4);
                hi.setText("Hi Score : " + hs);
            }
        }

        a1.add("+");
        a1.add("-");
        a1.add("*");
        a1.add("/");
        n = 3;
        startGame3(n);
        timer = new CounterClass(180000, 1000);
        timer.start();
        final Button backspace = (Button)findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backspace();
            }
        });
        backspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                arrayList.clear();
                TextView ans = (TextView) findViewById(R.id.answer);
                TextView res = (TextView) findViewById(R.id.result);
                ans.setText("");
                res.setText("");
                reset_all(1);
                return true;
            }
        });
        //movable();

        Button giveup = (Button)findViewById(R.id.giveup);
        touchListener = new MultiTouchListener(this);
        giveup.setOnTouchListener(touchListener);
    }

    public void giveUp(){
        timer.cancel();
        TextView scoret = (TextView)findViewById(R.id.textView3);
        TextView answer = (TextView)findViewById(R.id.answer);
        answer.setText("");
        a4.clear();
        arrayList.clear();
        score -= 50;
        scoret.setText("Score : " + score);
        xtr = "";
        for(int i = 0; i<a3.size(); i++)
            xtr += a3.get(i)+" ";
        //Toast.makeText(getBaseContext(), arr.getText().toString()+"\n-50 points", Toast.LENGTH_SHORT).show();
        xtr = xtr.trim();
        a3.clear();
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        xtr1 = textView2.getText().toString();
        guAlert();
    }

    public void guAlert(){

        final int i = (Integer.parseInt(xtr1.substring(xtr1.indexOf(":") + 2, xtr1.lastIndexOf(":"))) * 60 + Integer.parseInt(xtr1.substring(xtr1.lastIndexOf(":")+1))) * 1000;

        TextView target = (TextView)findViewById(R.id.target);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Solution :");
        alertDialog.setMessage("\t\t" + xtr + "  =  " + target.getText());
        alertDialog.setCancelable(false);
        alertX = 1;
        alertDialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Button b1 = (Button) findViewById(R.id.button1);
                Button b2 = (Button) findViewById(R.id.button2);
                Button b3 = (Button) findViewById(R.id.button3);
                Button b4 = (Button) findViewById(R.id.button4);
                Button b5 = (Button) findViewById(R.id.button5);
                Button b6 = (Button) findViewById(R.id.button6);
                Button b7 = (Button) findViewById(R.id.button7);
                Button b8 = (Button) findViewById(R.id.button8);
                Button b9 = (Button) findViewById(R.id.button9);
                Button b10 = (Button) findViewById(R.id.button10);
                b1.setVisibility(View.VISIBLE);
                b2.setVisibility(View.VISIBLE);
                b3.setVisibility(View.VISIBLE);
                b4.setVisibility(View.VISIBLE);
                b5.setVisibility(View.VISIBLE);
                b6.setVisibility(View.VISIBLE);
                b7.setVisibility(View.VISIBLE);
                b8.setVisibility(View.VISIBLE);
                b9.setVisibility(View.VISIBLE);
                b10.setVisibility(View.VISIBLE);
                a2.clear();
                alertX = 0;
                xtr = "";
                xtr1 = "";
                changeColour();
                startGame3(n);
                timer = new CounterClass(i, 1000);
                timer.start();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void startGame3(int n) {
        arrayList.clear();
        reset_all(1);
        Random rn = new Random();
        int r,i;
        for(i=0; i<n; i++)
        {
            r = rn.nextInt(9)+1;
            String s = Integer.toString(r);
            arrayList.add(s);
        }

        Button b1 = (Button)findViewById(R.id.button1);
        Button b2 = (Button)findViewById(R.id.button2);
        Button b3 = (Button)findViewById(R.id.button3);
        Button b4 = (Button)findViewById(R.id.button4);
        Button b5 = (Button)findViewById(R.id.button5);
        Button b6 = (Button)findViewById(R.id.button6);
        Button b7 = (Button)findViewById(R.id.button7);
        Button b8 = (Button)findViewById(R.id.button8);
        Button b9 = (Button)findViewById(R.id.button9);
        Button b10 = (Button)findViewById(R.id.button10);
        Button b11 = (Button)findViewById(R.id.button11);
        Button b12 = (Button)findViewById(R.id.button12);
        Button b13 = (Button)findViewById(R.id.button13);
        Button b14 = (Button)findViewById(R.id.button14);
        if(!arrayList.contains("1"))
            b1.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("2"))
            b2.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("3"))
            b3.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("4"))
            b4.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("5"))
            b5.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("6"))
            b6.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("7"))
            b7.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("8"))
            b8.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("9"))
            b9.setVisibility(View.INVISIBLE);
        if(!arrayList.contains("0"))
            b10.setVisibility(View.INVISIBLE);
        b11.setEnabled(false);
        b12.setEnabled(false);
        b13.setEnabled(false);
        b14.setEnabled(false);

        a2 = new ArrayList<String>(arrayList);
        Collections.sort(a2);
        Set<String> hs = new HashSet<>();
        hs.addAll(a2);
        a2.clear();
        a2.addAll(hs);

        i = 0;
        for(int k=0; k<n-1; k++)
        {
            r = rn.nextInt(4);
            if(a1.get(r).equals("/"))
            {
                if(Integer.parseInt(arrayList.get(i))%Integer.parseInt(arrayList.get(i+1)) == 0)
                {
                    arrayList.add(i+1,a1.get(r));
                }
                else
                {
                    r = rn.nextInt(3);
                    if(arrayList.size()>n && arrayList.get(i-1).equals("*")){
                        if(a1.get(r).equals("*")){
                            r = rn.nextInt(2);
                            arrayList.add(i+1,a1.get(r));
                        }
                        else
                            arrayList.add(i+1,a1.get(r));
                    }
                    else
                        arrayList.add(i+1,a1.get(r));
                }
            }
            else
            {
                if(arrayList.size()>n && arrayList.get(i-1).equals("*")){
                    if(a1.get(r).equals("*")){
                        r = rn.nextInt(2);
                        arrayList.add(i+1,a1.get(r));
                    }
                    else
                        arrayList.add(i+1,a1.get(r));
                }
                else
                    arrayList.add(i+1,a1.get(r));
            }
            i+=2;
        }
        a3 = new ArrayList<String>(arrayList);
        ans = calculate(arrayList);
        arrayList.clear();
        TextView target = (TextView)findViewById(R.id.target);
        target.setText(Integer.toString((int)ans));
    }

    public void playGame3(View v) {
        Button btn = (Button)v;
        TextView answer = (TextView)findViewById(R.id.answer);
        if(!btn.getText().toString().matches("\\W+")){
            a4.add(btn.getText().toString());
            Collections.sort(a4);
            reset_all(0);
        }
        else {
            reset_all(1);
        }
        arrayList.add(btn.getText().toString());
        answer.setText((answer.getText().toString() + " " + btn.getText().toString()).trim());
    }

    public void backspace() {
        if (arrayList.size() > 0) {
            arrayList.remove(arrayList.size() - 1);
            TextView ans = (TextView)findViewById(R.id.answer);
            String s;
            if (arrayList.size() > 1) {
                s = ans.getText().toString();
                int x = s.lastIndexOf(" ");
                s = s.substring(0, x);
                ans.setText(s.trim());
            }
            else if (arrayList.size()==1) {
                ans.setText(arrayList.get(0));
            }
            else {
                ans.setText("");
            }
            if(arrayList.size()>0) {
                if (!arrayList.get(arrayList.size() - 1).matches("\\W+")) {
                    reset_all(0);
                } else {
                    reset_all(1);
                }
            }
            else
                reset_all(1);
        }
    }

    public void cal(View v) {
        TextView answer = (TextView)findViewById(R.id.answer);
        if(arrayList.size()==0)
            Toast.makeText(getBaseContext(), "Please Enter Something", Toast.LENGTH_SHORT).show();
        else if(arrayList.get(arrayList.size()-1).equals("+") || arrayList.get(arrayList.size()-1).equals("-") || arrayList.get(arrayList.size()-1).equals("*") || arrayList.get(arrayList.size()-1).equals("/"))
            Toast.makeText(getBaseContext(), "Please Check Your Input", Toast.LENGTH_SHORT).show();
        else {
            if ((int) ans == (int) calculate(arrayList)) {
                Set<String> hs = new HashSet<>();
                hs.addAll(a4);
                a4.clear();
                a4.addAll(hs);
                if (a2.equals(a4)) {
                    counter++;
                    a2.clear();
                    a3.clear();
                    a4.clear();
                    arrayList.clear();
                    if (counter % 4 == 0)
                        n++;
                    score += 100;
                    Toast.makeText(getBaseContext(), "Good Job!!!", Toast.LENGTH_SHORT).show();
                    TextView scoret = (TextView) findViewById(R.id.textView3);
                    answer.setText("");
                    scoret.setText("Score : " + score);
                    Button b1 = (Button) findViewById(R.id.button1);
                    Button b2 = (Button) findViewById(R.id.button2);
                    Button b3 = (Button) findViewById(R.id.button3);
                    Button b4 = (Button) findViewById(R.id.button4);
                    Button b5 = (Button) findViewById(R.id.button5);
                    Button b6 = (Button) findViewById(R.id.button6);
                    Button b7 = (Button) findViewById(R.id.button7);
                    Button b8 = (Button) findViewById(R.id.button8);
                    Button b9 = (Button) findViewById(R.id.button9);
                    Button b10 = (Button) findViewById(R.id.button10);
                    b1.setVisibility(View.VISIBLE);
                    b2.setVisibility(View.VISIBLE);
                    b3.setVisibility(View.VISIBLE);
                    b4.setVisibility(View.VISIBLE);
                    b5.setVisibility(View.VISIBLE);
                    b6.setVisibility(View.VISIBLE);
                    b7.setVisibility(View.VISIBLE);
                    b8.setVisibility(View.VISIBLE);
                    b9.setVisibility(View.VISIBLE);
                    b10.setVisibility(View.VISIBLE);
                    changeColour();
                    startGame3(n);
                } else {
                    Toast.makeText(getBaseContext(), "You didn't used all the values shown. Please Try once more.", Toast.LENGTH_LONG).show();
                    arrayList = new ArrayList<String>(Arrays.asList(answer.getText().toString().split(" ")));
                }
            } else {
                Toast.makeText(getBaseContext(), "Calculated value didn't matched with the Tagtet value. Try Again!!!", Toast.LENGTH_LONG).show();
                arrayList = new ArrayList<String>(Arrays.asList(answer.getText().toString().split(" ")));
            }
        }
    }

    public class MultiTouchListener implements View.OnTouchListener {

        private float mPrevX;
        private float mPrevY;
        private static final int MAX_CLICK_DURATION = 200;//!!
        private long startClickTime; //!!

        public Game3 game3;
        public MultiTouchListener(Game3 game31) {
            game3 = game31;
        }

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            int action = event.getActionMasked();

            Button gvup = (Button)findViewById(R.id.giveup);
            gvup.setBackground(getResources().getDrawable(R.drawable.btn));

            switch (action ) {
                case MotionEvent.ACTION_DOWN: {
                    mPrevX = view.getX() - event.getRawX();
                    mPrevY = view.getY() - event.getRawY();
                    startClickTime = Calendar.getInstance().getTimeInMillis();//!!
                    gvup.setBackground(getResources().getDrawable(R.drawable.btn1));
                    break;
                }
                case MotionEvent.ACTION_MOVE:
                {
                    view.animate()
                        .x(event.getRawX() + mPrevX)
                        .y(event.getRawY() + mPrevY)
                        .setDuration(0)
                        .start();
                    gvup.setBackground(getResources().getDrawable(R.drawable.btn1));
                    break;
                }
                case MotionEvent.ACTION_CANCEL:
                    gvup.setBackground(getResources().getDrawable(R.drawable.btn1));
                    break;
                case MotionEvent.ACTION_UP:
                    long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                    if(clickDuration < MAX_CLICK_DURATION) {
                        //click event has occurred
                        gvup.setBackground(getResources().getDrawable(R.drawable.btn));
                        giveUp();
                    }
                    break;
            }
            return true;
        }
    }

    public void exitAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Exit");

        // Setting Dialog Message
        if(this.getClass().getSimpleName().equals("Game1") || this.getClass().getSimpleName().equals("Game1_1")) {
            alertDialog.setMessage("Are you sure to stop playing and quit the Game?");
        }
        else {
            alertDialog.setMessage("Do you want to quit the Game?");
        }

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(!this.getClass().getSimpleName().equals("MainActivity2")) {
                    timer.cancel();
                    Intent intent = new Intent(Game3.this, GameMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.putExtra("close", true);
                    startActivity(intent);
                    finish();
                }
                kill();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy The Game", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void offsetChk() {
        Button b1 = (Button)findViewById(R.id.button1);
        Button b2 = (Button)findViewById(R.id.button2);
        Button b3 = (Button)findViewById(R.id.button3);
        Button b4 = (Button)findViewById(R.id.button4);
        Button b5 = (Button)findViewById(R.id.button5);
        Button b6 = (Button)findViewById(R.id.button6);
        Button b7 = (Button)findViewById(R.id.button7);
        Button b8 = (Button)findViewById(R.id.button8);
        Button b9 = (Button)findViewById(R.id.button9);
        Button b10 = (Button)findViewById(R.id.button10);
        b1.setVisibility(View.VISIBLE);
        b2.setVisibility(View.VISIBLE);
        b3.setVisibility(View.VISIBLE);
        b4.setVisibility(View.VISIBLE);
        b5.setVisibility(View.VISIBLE);
        b6.setVisibility(View.VISIBLE);
        b7.setVisibility(View.VISIBLE);
        b8.setVisibility(View.VISIBLE);
        b9.setVisibility(View.VISIBLE);
        b10.setVisibility(View.VISIBLE);
        if(!a2.contains("1"))
            b1.setVisibility(View.INVISIBLE);
        if(!a2.contains("2"))
            b2.setVisibility(View.INVISIBLE);
        if(!a2.contains("3"))
            b3.setVisibility(View.INVISIBLE);
        if(!a2.contains("4"))
            b4.setVisibility(View.INVISIBLE);
        if(!a2.contains("5"))
            b5.setVisibility(View.INVISIBLE);
        if(!a2.contains("6"))
            b6.setVisibility(View.INVISIBLE);
        if(!a2.contains("7"))
            b7.setVisibility(View.INVISIBLE);
        if(!a2.contains("8"))
            b8.setVisibility(View.INVISIBLE);
        if(!a2.contains("9"))
            b9.setVisibility(View.INVISIBLE);
        if(!a2.contains("0"))
            b10.setVisibility(View.INVISIBLE);
        if(arrayList.size()>0) {
            if (!arrayList.get(arrayList.size() - 1).matches("\\W+")) {
                reset_all(0);
            } else {
                reset_all(1);
            }
        }
        else
            reset_all(1);
    }

    Bundle b = new Bundle();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        String x1 = textView2.getText().toString();
        int time = (Integer.parseInt(x1.substring(x1.indexOf(":") + 2, x1.lastIndexOf(":"))) * 60 + Integer.parseInt(x1.substring(x1.lastIndexOf(":")+1))) * 1000;
        outState.putInt("timer", time);
        b.putInt("timer",time);
        outState.putString("timert", textView2.getText().toString());
        timer.cancel();
        outState.putInt("score", score);
        TextView t = (TextView)findViewById(R.id.target);
        outState.putString("target", t.getText().toString());
        TextView scr = (TextView)findViewById(R.id.textView3);
        outState.putString("scoret", scr.getText().toString());
        outState.putStringArrayList("arrayList", arrayList);
        outState.putStringArrayList("a1", a1);
        outState.putStringArrayList("a2", a2);
        outState.putStringArrayList("a3", a3);
        outState.putStringArrayList("a4", a4);
        TextView ans = (TextView)findViewById(R.id.answer);
        TextView res = (TextView)findViewById(R.id.result);
        outState.putString("ans", ans.getText().toString());
        outState.putString("res", res.getText().toString());
        outState.putInt("color", color);
        outState.putInt("timc", timc);
        outState.putInt("alertX", alertX);
        outState.putString("xtr", xtr);
        outState.putString("xtr1", xtr1);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView t = (TextView)findViewById(R.id.target);
        TextView scr = (TextView)findViewById(R.id.textView3);
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        t.setText(savedInstanceState.getString("target"));
        ans = Double.parseDouble(savedInstanceState.getString("target"));
        score = savedInstanceState.getInt("score");
        color = savedInstanceState.getInt("color");
        timc = savedInstanceState.getInt("timc");
        scr.setText(savedInstanceState.getString("scoret"));
        textView2.setText(savedInstanceState.getString("timert"));
        arrayList = new ArrayList<String>(savedInstanceState.getStringArrayList("arrayList"));
        a1 = new ArrayList<String>(savedInstanceState.getStringArrayList("a1"));
        a2 = new ArrayList<String>(savedInstanceState.getStringArrayList("a2"));
        a3 = new ArrayList<String>(savedInstanceState.getStringArrayList("a3"));
        a4 = new ArrayList<String>(savedInstanceState.getStringArrayList("a4"));
        TextView ans = (TextView)findViewById(R.id.answer);
        TextView res = (TextView)findViewById(R.id.result);
        ans.setText(savedInstanceState.getString("ans"));
        res.setText(savedInstanceState.getString("res"));
        timeX = savedInstanceState.getInt("timer");
        timer.cancel();
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.background);
        rl.setBackgroundColor(color);
        int col = (0xFFFFFFFF - color) + (0xFF000000 & color);
        scr.setTextColor(col);
        textView2.setTextColor(timc);
        alertX = savedInstanceState.getInt("alertX");
        xtr = savedInstanceState.getString("xtr");
        xtr1 = savedInstanceState.getString("xtr1");
        offsetChk();
        if(alertX==1) {
            guAlert();
        }
        else {
            timer = new CounterClass(timeX, 1000);
            timer.start();
        }
        textView2.setTextColor(timc);
        savedInstanceState.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        timeX = b.getInt("timer");
        if(timeX>0) {
            timer = new CounterClass(timeX, 1000);
            timer.start();
        }
    }

    @Override
    public void onBackPressed() {
        alertTimer(0,timer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        actionBar(item);
        return true;
    }

    public boolean actionBar(MenuItem item){
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getBaseContext(), "You are in the middle of the game. Go to menu and check for instructions.", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.action_main) {
            alertTimer(0,timer);
            return true;
        }
        if (id == R.id.action_game_main) {
            alertTimer(1,timer);
            return true;
        }
        if (id == R.id.action_exit) {
            exitAlert();
            return true;
        }
        return true;
    }
}
