package com.example.sourav242.calmath;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

@TargetApi(Build.VERSION_CODES.FROYO)
@SuppressLint("NewApi")

public class Game2 extends GameMenu {

    int i,min = 70, max = 500;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> a1 = new ArrayList<>();
    CounterClass timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game2);

        DatabaseOperations DB = new DatabaseOperations(ctx);
        int c = DB.getCount();
        if(c>0) {
            Cursor cr = DB.getInformation(DB);
            cr.moveToFirst();
            if(c==1 && "game2".equals(cr.getString(0))) {
                String hs = "";
                do {
                    if ("game2".equals(cr.getString(0))) {
                        hs = cr.getString(1);
                    }
                } while (cr.moveToNext());
                TextView hi = (TextView) findViewById(R.id.textView4);
                hi.setText("Hi Score : " + hs);
            }
            if(c==2){
                String hs = "";
                do {
                    if ("game2".equals(cr.getString(0))) {
                        hs = cr.getString(1);
                    }
                } while (cr.moveToNext());
                TextView hi = (TextView) findViewById(R.id.textView4);
                hi.setText("Hi Score : " + hs);
            }
        }

        startGame2();

        timer = new CounterClass(180000, 1000);
        timer.start();
        //movable();

        final Button backspace = (Button)findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backspace();
            }
        });
        backspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                a1.clear();
                TextView ans = (TextView) findViewById(R.id.answer);
                TextView res = (TextView) findViewById(R.id.result);
                ans.setText("");
                res.setText("");
                reset_all(1);
                return true;
            }
        });
    }

    public void startGame2(){
        Random rn = new Random();
        i = rn.nextInt(max)+1;
        if(i < min)
        {
            i+=min;
        }
        TextView target = (TextView)findViewById(R.id.target);
        target.setText(Integer.toString(i));
        reset_all(1);
    }

    public void offsetChk() {
        Button b1 = (Button)findViewById(R.id.button1);
        Button b2 = (Button)findViewById(R.id.button2);
        Button b3 = (Button)findViewById(R.id.button3);
        Button b4 = (Button)findViewById(R.id.button4);
        Button b5 = (Button)findViewById(R.id.button5);
        Button b6 = (Button)findViewById(R.id.button6);
        Button b7 = (Button)findViewById(R.id.button7);
        Button b8 = (Button)findViewById(R.id.button8);
        Button b9 = (Button)findViewById(R.id.button9);
        Button b10 = (Button)findViewById(R.id.button10);

        if(a1.size()>0) {
            if (!a1.get(a1.size() - 1).matches("\\W+")) {
                reset_all(0);
            }
            else {
                if(a1.contains("1"))
                    b1.setEnabled(false);
                if(a1.contains("2"))
                    b2.setEnabled(false);
                if(a1.contains("3"))
                    b3.setEnabled(false);
                if(a1.contains("4"))
                    b4.setEnabled(false);
                if(a1.contains("5"))
                    b5.setEnabled(false);
                if(a1.contains("6"))
                    b6.setEnabled(false);
                if(a1.contains("7"))
                    b7.setEnabled(false);
                if(a1.contains("8"))
                    b8.setEnabled(false);
                if(a1.contains("9"))
                    b9.setEnabled(false);
                if(a1.contains("0"))
                    b10.setEnabled(false);
            }
        }
        else
            reset_all(1);
    }

    public void playGame2 (View v){
        TextView ans = (TextView)findViewById(R.id.answer);
        Button btn = (Button)v;
        if(!btn.getText().toString().matches("\\W+")){
            reset_all(0);
            ans.setText((ans.getText().toString() + " " + btn.getText().toString()).trim());
            a1.add(btn.getText().toString());
            gameFunc();
        }
        else {
            reset_all(1);
            ans.setText(ans.getText().toString() + " " + btn.getText().toString());
            a1.add(btn.getText().toString());
        }
        Button b1 = (Button)findViewById(R.id.button1);
        Button b2 = (Button)findViewById(R.id.button2);
        Button b3 = (Button)findViewById(R.id.button3);
        Button b4 = (Button)findViewById(R.id.button4);
        Button b5 = (Button)findViewById(R.id.button5);
        Button b6 = (Button)findViewById(R.id.button6);
        Button b7 = (Button)findViewById(R.id.button7);
        Button b8 = (Button)findViewById(R.id.button8);
        Button b9 = (Button)findViewById(R.id.button9);
        Button b10 = (Button)findViewById(R.id.button10);
        if(a1.contains("1"))
            b1.setEnabled(false);
        if(a1.contains("2"))
            b2.setEnabled(false);
        if(a1.contains("3"))
            b3.setEnabled(false);
        if(a1.contains("4"))
            b4.setEnabled(false);
        if(a1.contains("5"))
            b5.setEnabled(false);
        if(a1.contains("6"))
            b6.setEnabled(false);
        if(a1.contains("7"))
            b7.setEnabled(false);
        if(a1.contains("8"))
            b8.setEnabled(false);
        if(a1.contains("9"))
            b9.setEnabled(false);
        if(a1.contains("0"))
            b10.setEnabled(false);
    }

    public void gameFunc() {
        TextView result = (TextView)findViewById(R.id.result);
        double x;
        arrayList = new ArrayList<String>(a1);
        if(arrayList.size()==1)
            x = Integer.parseInt(arrayList.get(0));
        else
            x = calculate(arrayList);
        if(Double.isInfinite(x))
            result.setText(Double.toString(x));
        else
            result.setText(Integer.toString((int)x));
        if(i == x)
        {
            Toast.makeText(getBaseContext(), "Good Job!!!", Toast.LENGTH_SHORT).show();
            TextView answer = (TextView)findViewById(R.id.answer);
            TextView scoreText = (TextView)findViewById(R.id.textView3);
            score += 100;
            scoreText.setText("Score : "+(score));
            result.setText("Good Job!!!");
            arrayList.clear();
            a1.clear();
            answer.setText("");
            changeColour();
            startGame2();
        }
    }

    public void backspace() {
        if (a1.size() > 0) {
            a1.remove(a1.size() - 1);
            if (a1.size() > 1 && a1.get(a1.size() - 1).matches("\\W+")) {
                ArrayList<String> a2 = new ArrayList<>();
                a2 = new ArrayList<String>(a1);
                a1.remove(a1.size() - 1);
                gameFunc();
                a1 = new ArrayList<String>(a2);
            }
            else if(a1.size() > 1 && !a1.get(a1.size() - 1).matches("\\W+"))
                gameFunc();
            TextView ans = (TextView) findViewById(R.id.answer);
            String s;
            if (a1.size() > 1) {
                s = ans.getText().toString();
                int x = s.lastIndexOf(" ");
                s = s.substring(0, x);
                ans.setText(s.trim());
            }
            else if (a1.size()==1) {
                ans.setText(a1.get(0));
                TextView res = (TextView) findViewById(R.id.result);
                res.setText(a1.get(0));
            }
            else {
                ans.setText("");
                TextView res = (TextView) findViewById(R.id.result);
                res.setText("");
            }
            Button b1 = (Button)findViewById(R.id.button1);
            Button b2 = (Button)findViewById(R.id.button2);
            Button b3 = (Button)findViewById(R.id.button3);
            Button b4 = (Button)findViewById(R.id.button4);
            Button b5 = (Button)findViewById(R.id.button5);
            Button b6 = (Button)findViewById(R.id.button6);
            Button b7 = (Button)findViewById(R.id.button7);
            Button b8 = (Button)findViewById(R.id.button8);
            Button b9 = (Button)findViewById(R.id.button9);
            Button b10 = (Button)findViewById(R.id.button10);
            Button b11 = (Button)findViewById(R.id.button11);
            Button b12 = (Button)findViewById(R.id.button12);
            Button b13 = (Button)findViewById(R.id.button13);
            Button b14 = (Button)findViewById(R.id.button14);

            if(a1.size()>0) {
                if (!a1.get(a1.size() - 1).matches("\\W+")) {
                    reset_all(0);
                } else {

                    if(!a1.contains("1"))
                        b1.setEnabled(true);
                    if(!a1.contains("2"))
                        b2.setEnabled(true);
                    if(!a1.contains("3"))
                        b3.setEnabled(true);
                    if(!a1.contains("4"))
                        b4.setEnabled(true);
                    if(!a1.contains("5"))
                        b5.setEnabled(true);
                    if(!a1.contains("6"))
                        b6.setEnabled(true);
                    if(!a1.contains("7"))
                        b7.setEnabled(true);
                    if(!a1.contains("8"))
                        b8.setEnabled(true);
                    if(!a1.contains("9"))
                        b9.setEnabled(true);
                    if(!a1.contains("0"))
                        b10.setEnabled(true);
                    b11.setEnabled(false);
                    b12.setEnabled(false);
                    b13.setEnabled(false);
                    b14.setEnabled(false);
                }
            }
            else
                reset_all(1);
        }
    }

    public void exitAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Exit");

        // Setting Dialog Message
        if(this.getClass().getSimpleName().equals("Game1") || this.getClass().getSimpleName().equals("Game1_1")) {
            alertDialog.setMessage("Are you sure to stop playing and quit the Game?");
        }
        else {
            alertDialog.setMessage("Do you want to quit the Game?");
        }

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(!this.getClass().getSimpleName().equals("MainActivity2")) {
                    timer.cancel();
                    Intent intent = new Intent(Game2.this, GameMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.putExtra("close", true);
                    startActivity(intent);
                    finish();
                }
                kill();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy The Game", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    Bundle b = new Bundle();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        String x1 = textView2.getText().toString();
        int time = (Integer.parseInt(x1.substring(x1.indexOf(":") + 2, x1.lastIndexOf(":"))) * 60 + Integer.parseInt(x1.substring(x1.lastIndexOf(":")+1))) * 1000;
        outState.putInt("timer", time);
        b.putInt("timer",time);
        outState.putString("timert", textView2.getText().toString());
        timer.cancel();
        outState.putInt("score", score);
        TextView t = (TextView)findViewById(R.id.target);
        outState.putString("target", t.getText().toString());
        TextView scr = (TextView)findViewById(R.id.textView3);
        outState.putString("scoret",scr.getText().toString());
        outState.putStringArrayList("a1", a1);
        TextView ans = (TextView)findViewById(R.id.answer);
        TextView res = (TextView)findViewById(R.id.result);
        outState.putString("ans",ans.getText().toString());
        outState.putString("res",res.getText().toString());
        outState.putInt("color", color);
        outState.putInt("timc",timc);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView t = (TextView)findViewById(R.id.target);
        TextView scr = (TextView)findViewById(R.id.textView3);
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        t.setText(savedInstanceState.getString("target"));
        i = Integer.parseInt(savedInstanceState.getString("target"));
        score = savedInstanceState.getInt("score");
        color = savedInstanceState.getInt("color");
        timc = savedInstanceState.getInt("timc");
        scr.setText(savedInstanceState.getString("scoret"));
        textView2.setText(savedInstanceState.getString("timert"));
        a1 = new ArrayList<String>(savedInstanceState.getStringArrayList("a1"));
        TextView ans = (TextView)findViewById(R.id.answer);
        TextView res = (TextView)findViewById(R.id.result);
        ans.setText(savedInstanceState.getString("ans"));
        res.setText(savedInstanceState.getString("res"));
        offsetChk();
        timeX = savedInstanceState.getInt("timer");
        timer.cancel();

        RelativeLayout rl = (RelativeLayout)findViewById(R.id.background);
        rl.setBackgroundColor(color);
        int col = (0xFFFFFFFF - color) + (0xFF000000 & color);
        scr.setTextColor(col);
        textView2.setTextColor(timc);
        timer = new CounterClass(timeX, 1000);
        timer.start();
        savedInstanceState.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        timeX = b.getInt("timer");
        if(timeX>0) {
            timer = new CounterClass(timeX, 1000);
            timer.start();
        }
    }

    @Override
    public void onBackPressed() {
        alertTimer(0, timer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.f
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        actionBar(item);
        return true;
    }

    public boolean actionBar(MenuItem item){
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getBaseContext(), "You are in the middle of the game. Go to menu and check for instructions.", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.action_main) {
            alertTimer(0,timer);
            return true;
        }
        if (id == R.id.action_game_main) {
            alertTimer(1,timer);
            return true;
        }
        if (id == R.id.action_exit) {
            exitAlert();
            return true;
        }
        return true;
    }
}
