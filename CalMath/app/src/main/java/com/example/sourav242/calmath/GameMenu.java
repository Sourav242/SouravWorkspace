package com.example.sourav242.calmath;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GameMenu extends AppCompatActivity {
    int c, score = 0, color = -1, timc;
    int timeX = 0;
    String packageName = "com.example.sourav242.calmath";
    Context ctx = this;
    int alertX = 0;
    String xtr = "",xtr1 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("close", false)) {
            kill();
        }
        else
            setContentView(R.layout.game_menu);
    }

    public void kill(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    public void onClickBtn(View v){
        Button btn = (Button)v;
        SessionClass sessionClass = (SessionClass)getApplicationContext();
        if(btn.getText().toString().contains("Get")) {
            Intent mainIntent = new Intent(this, GetThat20.class);
            startActivity(mainIntent);
        }
        else if (btn.getText().toString().contains("O")) {
            sessionClass.setPlayer("game3");
            Intent mainIntent = new Intent(this, Sign_O_Phobia.class);
            startActivity(mainIntent);
        }
        else {
            sessionClass.setPlayer("game2");
            Intent mainIntent = new Intent(this, CalculateToTheTarget.class);
            startActivity(mainIntent);
        }
    }

    public double calculate(ArrayList<String> arrayList) {
        double calc = 0;
        c = arrayList.size();
        try
        {
            while (c != 1)
            {
                if (c > 3)
                {
                    if (arrayList.get(3).equals("*") || arrayList.get(3).equals("/"))
                    {
                        if(arrayList.get(1).equals("*") || arrayList.get(1).equals("/"))
                        {
                            calc = calc1(arrayList);
                        }
                        else
                        {
                            if (arrayList.get(3).equals("*")) {
                                calc = Double.parseDouble(arrayList.get(2)) * Double.parseDouble(arrayList.get(4));
                            }
                            if (arrayList.get(3).equals("/")) {
                                calc = Double.parseDouble(arrayList.get(2)) / Double.parseDouble(arrayList.get(4));
                            }
                            arrayList.remove(2);
                            arrayList.remove(2);
                            arrayList.remove(2);
                            arrayList.add(2, (Double.toString(calc)));
                            c = arrayList.size();
                        }
                    }
                    else
                    {
                        calc = calc1(arrayList);
                    }
                }
                else
                {
                    calc = calc1(arrayList);
                }
            }
            return calc;
        }
        catch (IllegalStateException e)
        {
            arrayList.clear();
            Toast.makeText(getBaseContext(), "Wrong Input!!!", Toast.LENGTH_SHORT).show();
            return 0;
        }
    }

    public double calc1(ArrayList<String> arrayList){
        double calc = 0;
        if (arrayList.get(1).equals("+")) {
            calc = Double.parseDouble(arrayList.get(0)) + Double.parseDouble(arrayList.get(2));
        }
        if (arrayList.get(1).equals("-")) {
            calc = Double.parseDouble(arrayList.get(0)) - Double.parseDouble(arrayList.get(2));
        }
        if (arrayList.get(1).equals("*")) {
            calc = Double.parseDouble(arrayList.get(0)) * Double.parseDouble(arrayList.get(2));
        }
        if (arrayList.get(1).equals("/")) {
            calc = Double.parseDouble(arrayList.get(0)) / Double.parseDouble(arrayList.get(2));
        }
        arrayList.remove(0);
        arrayList.remove(0);
        arrayList.remove(0);
        arrayList.add(0, Double.toString(calc));
        c = arrayList.size();
        return calc;
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if(this.getClass().getSimpleName().equals("GameMenu")) {
            if (doubleBackToExitPressedOnce) {
                kill();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
        else
            super.onBackPressed();
    }

    public void changeColour() {
        Random rnd = new Random();
        int r = rnd.nextInt(256);
        int g = rnd.nextInt(256);
        int b = rnd.nextInt(256);
        color = Color.argb(255, r < 100 ? r + 100 : r, g < 100 ? g + 100 : g, b < 100 ? b + 100 : b);
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.background);
        TextView scoreText = (TextView)findViewById(R.id.textView3);
        rl.setBackgroundColor(color);
        int col = (0xFFFFFFFF - color) + (0xFF000000 & color);
        scoreText.setTextColor(col);
    }

    public void exitAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Exit");

        // Setting Dialog Message
        if(this.getClass().getSimpleName().equals("Game1") || this.getClass().getSimpleName().equals("Game1_1")) {
            alertDialog.setMessage("Are you sure to stop playing and quit the Game?");
        }
        else {
            alertDialog.setMessage("Do you want to quit the Game?");
        }

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(!this.getClass().getSimpleName().equals("MainActivity2")) {
                    Intent intent = new Intent(GameMenu.this, GameMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.putExtra("close", true);
                    startActivity(intent);
                    finish();
                }
                kill();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy The Game", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void alertBack(String x){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Wait!!!");
        alertDialog.setMessage("Are you sure to "+x+" the game?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy the Game", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public String dbEntry(String s) {
        DatabaseOperations DB = new DatabaseOperations(ctx);
        int c = DB.getCount();
        if(c==0){
            DB.putInformation(DB, s, Integer.toString(score));
        }
        else {
            Cursor cr = DB.getInformation(DB);
            cr.moveToFirst();
            if(c==1 && !s.equals(cr.getString(0)))
                DB.putInformation(DB, s, Integer.toString(score));
            else
                do{
                    if(s.equals(cr.getString(0)) && score>Integer.parseInt(cr.getString(1))) {
                        DB.updateUserInfo(DB,cr.getString(0),cr.getString(1),cr.getString(0),Integer.toString(score));
                    }
                }while(cr.moveToNext());
        }
        Cursor cr = DB.getInformation(DB);
        cr.moveToFirst();
        do{
            if(s.equals(cr.getString(0))){
                return cr.getString(1);
            }
        }while(cr.moveToNext());
        return Integer.toString(score);
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {

        TextView timertext = (TextView)findViewById(R.id.textView2);
        int tick = 0;
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            // TODO Auto-generated constructor stub
        }

        @SuppressLint("NewApi")
        @TargetApi(Build.VERSION_CODES.FROYO)
        @Override
        public void onTick(long millis) {
            // TODO Auto-generated method stub

            String ms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            tick++;
            if(timertext.getText().toString().equals("03:00")) {
                timc = Color.parseColor("#388E3C");
                timertext.setTextColor(timc);
            }
            if(tick%2 == 0)
                timertext.setText("Tok : "+ms);
            else
                timertext.setText("Tik : " + ms);
            if(timertext.getText().toString().contains("k : 01:30")) {
                timc = Color.parseColor("#FCD914");
                timertext.setTextColor(timc);
            }
            if(timertext.getText().toString().contains("k : 01:00")) {
                timc = Color.parseColor("#FFAA47");
                timertext.setTextColor(timc);
            }
            if(timertext.getText().toString().contains("k : 00:30")) {
                timc = Color.parseColor("#ff0000");
                timertext.setTextColor(timc);
            }
        }

        @Override
        public void onFinish() {
            timertext.setText("Tok : 00:00");
            // TODO Auto-generated method stub
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(GameMenu.this);

            SessionClass sessionClass = (SessionClass)getApplicationContext();
            final String str = sessionClass.getPlayer();

            String hi = dbEntry(str);

            // Setting Dialog Message
            alertDialog.setMessage("Your Score is : " + score +"\nHi Score is : " + hi);

            final ImageView img = new ImageView(GameMenu.this);
            if(score>0) {
                // Setting Dialog Title
                alertDialog.setTitle("Congratulations...");
                img.setImageResource(R.drawable.firecrackers);
            }
            else {
                alertDialog.setTitle("Let's Play Again..");
                img.setImageResource(R.drawable.mmm);
            }

            alertDialog.setView(img);

            // Setting Dialog Cancellation
            alertDialog.setCancelable(false);

            // Setting Positive "Yes" Button

            alertDialog.setPositiveButton("Play Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    RelativeLayout r = (RelativeLayout)findViewById(R.id.background);
                    r.setBackgroundColor(Color.parseColor("#ffffff"));
                    Intent i;
                    if(null != str && str.contains("2"))
                        i = new Intent(GameMenu.this, Game2.class);
                    else
                        i = new Intent(GameMenu.this, Game3.class);
                    startActivity(i);
                    finish();
                    /*i = new Intent(GameMenu.this, Game3.class);
                    startActivity(i);
                    finish();*/
                }
            });

            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("Main Menu", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }
    }

    public void reset_all(int bol) {
        Button b1 = (Button)findViewById(R.id.button1);
        Button b2 = (Button)findViewById(R.id.button2);
        Button b3 = (Button)findViewById(R.id.button3);
        Button b4 = (Button)findViewById(R.id.button4);
        Button b5 = (Button)findViewById(R.id.button5);
        Button b6 = (Button)findViewById(R.id.button6);
        Button b7 = (Button)findViewById(R.id.button7);
        Button b8 = (Button)findViewById(R.id.button8);
        Button b9 = (Button)findViewById(R.id.button9);
        Button b10 = (Button)findViewById(R.id.button10);
        Button b11 = (Button)findViewById(R.id.button11);
        Button b12 = (Button)findViewById(R.id.button12);
        Button b13 = (Button)findViewById(R.id.button13);
        Button b14 = (Button)findViewById(R.id.button14);
        if(bol == 1) {
            b1.setEnabled(true);
            b2.setEnabled(true);
            b3.setEnabled(true);
            b4.setEnabled(true);
            b5.setEnabled(true);
            b6.setEnabled(true);
            b7.setEnabled(true);
            b8.setEnabled(true);
            b9.setEnabled(true);
            b10.setEnabled(true);
            b11.setEnabled(false);
            b12.setEnabled(false);
            b13.setEnabled(false);
            b14.setEnabled(false);
        }
        else {
            b1.setEnabled(false);
            b2.setEnabled(false);
            b3.setEnabled(false);
            b4.setEnabled(false);
            b5.setEnabled(false);
            b6.setEnabled(false);
            b7.setEnabled(false);
            b8.setEnabled(false);
            b9.setEnabled(false);
            b10.setEnabled(false);
            b11.setEnabled(true);
            b12.setEnabled(true);
            b13.setEnabled(true);
            b14.setEnabled(true);
        }
    }

    public void alertTimer(final int i,final CounterClass timer){
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Wait!!!");
        alertDialog.setMessage("Are you sure to exit the game?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                timer.cancel();
                if(i == 0)
                    finish();
                else{
                    Intent intent = new Intent(GameMenu.this, GameMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                }
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Enjoy the Game", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        actionBar(item);
        return super.onOptionsItemSelected(item);
    }

    public boolean actionBar(MenuItem item){
        int id = item.getItemId();

        Intent intent = new Intent();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if(this.getClass().getSimpleName().equals("CalculateToTheTarget")) {
                intent.setClassName(packageName, packageName + ".Instruction2");
                startActivity(intent);
            }
            if(this.getClass().getSimpleName().equals("Sign_O_Phobia")) {
                intent.setClassName(packageName, packageName + ".Instruction3");
                startActivity(intent);
            }
            if(this.getClass().getSimpleName().equals("MainActivity2") || this.getClass().getSimpleName().equals("Toss") || this.getClass().getSimpleName().equals("Game1") || this.getClass().getSimpleName().equals("Game1_1")) {
                intent.setClassName(packageName, packageName + ".Instruction");
                startActivity(intent);
            }
            if(this.getClass().getSimpleName().equals("GameMenu")) {
                intent.setClassName(packageName, packageName + ".WelcomeInstruction");
                startActivity(intent);
            }
            return true;
        }
        if (id == R.id.action_main) {
            if(this.getClass().getSimpleName().equals("GameMenu"))
                Toast.makeText(getBaseContext(), "Please select a game to move to its menu page.", Toast.LENGTH_SHORT).show();
            else if(this.getClass().getSimpleName().equals("Game1") || this.getClass().getSimpleName().equals("Game1_1") || this.getClass().getSimpleName().equals("Instruction")) {
                intent = new Intent(this, MainActivity2.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
            else if(this.getClass().getSimpleName().equals("Instruction2")) {
                intent = new Intent(this, CalculateToTheTarget.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
            else if(this.getClass().getSimpleName().equals("Instruction3")) {
                intent = new Intent(this, Sign_O_Phobia.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
            else if(this.getClass().getSimpleName().equals("Toss") || this.getClass().getSimpleName().equals("WelcomeInstruction"))
                finish();
            else
                Toast.makeText(getBaseContext(), "You are in Menu Page.", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.action_game_main) {
            if(this.getClass().getSimpleName().equals("GameMenu"))
                Toast.makeText(getBaseContext(), "You are in Game Select Menu.", Toast.LENGTH_SHORT).show();
            else if(this.getClass().getSimpleName().equals("CalculateToTheTarget") || this.getClass().getSimpleName().equals("MainActivity2") || this.getClass().getSimpleName().equals("Sign_O_Phobia") || this.getClass().getSimpleName().equals("WelcomeInstruction")) {
                finish();
            }
            else {
                intent = new Intent(this, GameMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
            return true;
        }
        if (id == R.id.action_exit) {
            exitAlert();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
