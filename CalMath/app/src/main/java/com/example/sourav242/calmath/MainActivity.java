package com.example.sourav242.calmath;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    Handler h = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            final Intent mainIntent = new Intent(MainActivity.this, GameMenu.class);
            startActivity(mainIntent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        h.postDelayed(r, 3000);
    }

    public void onSplash1(View view) {
        h.removeCallbacks(r);
        Intent mainIntent = new Intent(this, GameMenu.class);
        startActivity(mainIntent);
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        h.removeCallbacks(r);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        h.removeCallbacks(r);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        h.removeCallbacks(r);
        h.postDelayed(r, 500);
    }
}
