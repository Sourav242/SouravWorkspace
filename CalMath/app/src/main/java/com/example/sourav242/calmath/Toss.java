package com.example.sourav242.calmath;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Toss extends MainActivity2 {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toss);
        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.relativelayout1);
        relativeLayout.setBackgroundColor(Color.parseColor("#99cc22"));
    }

    public void clkStrtGm(View v){
        SessionClass Session = (SessionClass)getApplicationContext();
        Button btn = (Button)v;
        if(btn.getText().toString().equals("Start Game")) {
            Session.setMode("Reach 20");
            Intent i = new Intent(v.getContext(), Game1.class);
            startActivity(i);
        }
        if(btn.getText().toString().contains("Let")){
            Session.setMode("Let The Game Start");
            Intent i = new Intent(v.getContext(), Game1.class);
            startActivity(i);
        }
    }
}
