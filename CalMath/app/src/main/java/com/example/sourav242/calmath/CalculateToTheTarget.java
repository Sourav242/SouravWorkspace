package com.example.sourav242.calmath;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CalculateToTheTarget extends GameMenu {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculate_to_the_target);

        final Button play = (Button)findViewById(R.id.play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculateToTheTarget.this, Game2.class));
                finish();
            }
        });

        final Button inst = (Button)findViewById(R.id.inst);
        inst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalculateToTheTarget.this, Instruction2.class));
            }
        });
    }
}
